The Hemodialysis Machine Case Study

Authors:
Artur Gomes
Andrew Butterfield

The folder contains the following files:

  HDMAIN.PDF     		PDF containing the specification in Circus
  haemodel-v1.6.csp		File containing the translated version into
  						CSP to be checked in FDR
